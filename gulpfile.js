// grab our gulp packages
var gulp  = require('gulp');
var sass = require('gulp-sass');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');


gulp.task('default', function() {
    gulp.watch('assets/stylesheet/sass/**/*.scss', ['build-css', 'minify-css']);
});
gulp.task('build-css', function() {
    return gulp.src('assets/stylesheet/sass/main.scss')
      .pipe(sass())
      .pipe(gulp.dest('assets/stylesheet/css'));
  });
  
  gulp.task('minify-css', ['build-css'], function() {
    return gulp.src(['assets/stylesheet/css/*.css', '!assets/stylesheet/css/*.min.css'])
      .pipe(cssmin())
      .pipe(rename({suffix: '.min'})) 
      .pipe(gulp.dest('assets/stylesheet/css'));
  });
  